# ExpressJS + Fabcar #

This is a simple example for integration between ExpressJS and Hyperledger Fabric.


 
### Fabric Usage ###

```
./startFabric.sh
```

### ExpressJS ###

Local Terminal:
```
    * node install expressjs
    * node server.js
```
In browser: 
```
http://localhost/api/query -> use user created in the step before.
```

### Who do I talk to? ###
* Matias Kruk <kruk.matias at gmail.com>